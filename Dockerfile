FROM cloudron/base:3.0.0
MAINTAINER Dennis Schwerdel <schwerdel@googlemail.com>

ENV VERSION=3.9.3

RUN mkdir -p /app/code

WORKDIR /app/code

RUN curl -fsSL https://code-server.dev/install.sh | sh -s -- --version ${VERSION}
ADD config.yaml /app/code/default-config.yaml

RUN mv /home/cloudron /app/code/default-home \
 && ln -s /app/data /home/cloudron
WORKDIR /app/data
ENV HOME=/app/data

ADD start.sh /app/code/start.sh
ADD favicon /app/code/favicon
RUN cp /app/code/favicon/* /usr/lib/code-server/src/browser/media/

RUN usermod -aG sudo cloudron
Add chroot-alpine /usr/local/bin/chroot-alpine

CMD [ "/app/code/start.sh" ]
