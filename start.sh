#!/bin/bash

set -eu

mkdir -p /app/data/vscode/{config,data} /app/data/{.config,.local/share}

for file in $(ls /app/code/default-home -A); do
  if ! [ -f /app/data/$file ]; then
    cp -a /app/code/default-home/$file /app/data/$file
  fi
done

if ! [ -e /app/data/.config/code-server ]; then
  ln -s /app/data/vscode/config /app/data/.config/code-server
fi
if ! [ -e /app/data/.local/share/code-server ]; then
  ln -s /app/data/vscode/data /app/data/.local/share/code-server
fi
if ! [ -f /app/data/vscode/config/config.yaml ]; then
  cp /app/code/default-config.yaml /app/data/vscode/config/config.yaml
  PASSWORD=$(dd if=/dev/urandom bs=1 count=12 2>/dev/null | base64)
  sed -i "s/%PASSWORD%/${PASSWORD}/g" /app/data/vscode/config/config.yaml
  echo "Generated random password: ${PASSWORD}"
fi

if [ "${CLOUDRON_PROXY_AUTH:-0}" == "1" ]; then
  sed -i "s/^auth:.*/auth: none/g" /app/data/vscode/config/config.yaml
else
  sed -i "s/^auth:.*/auth: password/g" /app/data/vscode/config/config.yaml
fi

chown -R cloudron: /app/data

grep -e '^password' --color=never /app/data/vscode/config/config.yaml

exec sudo -u cloudron code-server
