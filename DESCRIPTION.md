## VS Code

Run VS Code on any machine anywhere and access it in the browser.

This app packages code-server (https://github.com/cdr/code-server), 
a Visual Studio Code version that runs directly in the browser.

The app comes with very little adaptations to code-server:

* Move all vscode folders to /app/data/vscode
* Listen on 0.0.0.0:8080
* Generate a randomized password on first start (similar to code-server)
* Print passwort to logs on each start
* Use original VS code icon as favicon
* Optional Cloudron SSO integration
* Enable sudo
* TCP/UDP Development Port
* Script to run chroot'ed Alpine Linux


### Changing the password (only for custom auth)

The password is configured in `/app/data/vscode/config/config.yaml`.
You can change it by modifying the `password` key.

Also you can set the `hashed-password` key (create it) to the SHA-256 checksum
of a password (This setting takes precedence over `password`).

After changing the password (either way), the app has to be restarted.


### Installing additional software

There are two ways to install required additional software:

1. You can install software into the home folder of the current user. This works for a lot of tools like NodeJS or Rust.

2. You can download an image of a full OS (e.g. Alpine Linux) and work inside it using chroot. See `chroot-alpine`.
