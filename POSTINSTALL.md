# App authentication

This app integrates with Cloudron SSO by default. 
If you choose to opt out of SSO, a custom password is used.
The initial password is randomly generated and printed to the logs when the app starts.

Please note that the app provides the same environment to all users.


## Changing the password (only for custom auth)

The password is configured in `/app/data/vscode/config/config.yaml`.
You can change it by modifying the `password` key.

Also you can set the `hashed-password` key (create it) to the SHA-256 checksum
of a password (This setting takes precedence over `password`).

After changing the password (either way), the app has to be restarted.

Make sure to set a secure password as VS code provides shell access to 
everyone who knows the password.
