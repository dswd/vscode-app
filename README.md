# VS Code Cloudron App

This repository contains the Cloudron app package source for VS Code.

## Installation

[![Install](https://cloudron.io/img/button.svg)](https://cloudron.io/button.html?app=com.coder.codeserver.cloudronapp)

or using the [Cloudron command line tooling](https://cloudron.io/references/cli.html)

```
cloudron install --appstore-id com.coder.codeserver.cloudronapp
```

## Building

The app package can be built using the [Cloudron command line tooling](https://cloudron.io/references/cli.html).

```
cd vscode-app

cloudron build
cloudron install
```
